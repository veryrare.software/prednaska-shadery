#ifndef ADDITIONA_LIGHTS_SHADOW_CUSTOM
#define ADDITIONA_LIGHTS_SHADOW_CUSTOM

void AdditionalLightsShadow_float(float3 WorldPosition, out float shadowAtt) {
   shadowAtt = 1;
#ifndef SHADERGRAPH_PREVIEW
   int pixelLightCount = GetAdditionalLightsCount();
   for (int i = 0; i < pixelLightCount; ++i) {
		#if VERSION_GREATER_EQUAL(10, 1)
			Light light = GetAdditionalLight(i, WorldPosition, half4(1,1,1,1));
		#else
			Light light = GetAdditionalLight(i, WorldPosition);
		#endif
        shadowAtt *= step(0.5, light.shadowAttenuation);
   }
#endif
}



#endif
